# iTunes Account Crawler - Calculate your iTunes spendings

Putting your compulsive hoarding of emails to use, this script will calculate the amount of money you have spent on Apple's iTunes Store, App Store, Mac App Store, iBook Store, and anything else being billed under the iTunes Store banner.

The script is written in Python (3) and is based in standard libraries only. As expected Apple's iTunes Store backend/accounting is just as much a walled garden as the frontend, the crawler can't use the data from the "Previous Purchases" / "Purchase Histoy" page of the iTunes Store. The only other way to get an overall total is to use the e-mailed invoices in the account owner's mailbox.

## Requirements

* Python 3
* A collection of invoices sent by the Apple iTunes Store
* Said invoices being on an IMAP mail server

## Usage

Simply download the repository and execute the script using python:

```
python3 itunes.py
```

All required parameters will be asked for, and the script will work its magic.

## License

Copyright (c) 2016 Jan Willhaus

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.